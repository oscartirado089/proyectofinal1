/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Models;
import Models.Citas;
import Models.Clientes;
import Models.dbManejador;
import Models.dbPersistencia;
import Views.dlgClientes;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;
/**
 *
 * @author _Enrique_
 */
public class dbCliente extends dbManejador implements dbPersistencia{
    dlgClientes clie;
    @Override
    public void insertar(Object objeto) throws Exception {
        Clientes user = new Clientes();
        user = (Clientes) objeto;
        String consulta = "insert into " + "clientes(nombres, apellidoPa,apellidoMa,telefono,direccion)" +"values (?, ?, ?, ?, ?)";
        if(this.conectar()){
            try{
                System.err.println("Se conecto");
                this.sqlConsulta = conexion.prepareStatement(consulta);
                //Asignar valores a la consulta
                //this.sqlConsulta.setInt(1, user.getIdCliente());
                this.sqlConsulta.setString(1, user.getNombres());
                this.sqlConsulta.setString(2, user.getApellidoPa());
                this.sqlConsulta.setString(3, user.getApellidoMa());
                this.sqlConsulta.setString(4, user.getTelefono());
                this.sqlConsulta.setString(5, user.getDireccion());
                this.sqlConsulta.executeUpdate();
                this.desconectar();
            }
            catch (SQLException e){
                System.err.println("Surgió un error al insertar" + e.getMessage());
            }
        }
        else{
            System.out.println("No fue posible conectarse ");
        }
    }

    @Override
    public void actualizar(Object objeto) throws Exception {
        Clientes user = new Clientes();
        user = (Clientes) objeto;
        String consulta = "update Clientes set nombres = ?, apellidoPa = ?, apellidoMa = ?, telefono = ?, direccion = ?, where id_Clientes = ?";
        if(this.conectar()){
            try{
                System.err.println("Se conecto");
                this.sqlConsulta = conexion.prepareStatement(consulta);
                //Asignar valores a la consulta
                
                this.sqlConsulta.setString(1, user.getNombres());
                this.sqlConsulta.setString(2, user.getApellidoPa());
                this.sqlConsulta.setString(3, user.getApellidoMa());             
                this.sqlConsulta.setString(4, user.getTelefono());
                this.sqlConsulta.setString(5, user.getDireccion());
                this.sqlConsulta.executeUpdate();
                 this.desconectar();
                JOptionPane.showMessageDialog(clie,"Se actualizó correctamente");

            }catch (SQLException e) {
                JOptionPane.showMessageDialog(clie,"Surgio un error al actualizar" +e.getMessage());
            }
        }
    }

    @Override
    public void borrar(Object objeto, String codigo) throws Exception {
        Clientes user = new Clientes();
        user = (Clientes) objeto;
        String consulta = "DELETE from clientes WHERE id_Clientes = ?";
                
        if(conectar()){
            try{
                System.err.println("Se conecto");
                 this.sqlConsulta = this.conexion.prepareStatement(consulta);
                

                this.sqlConsulta.setString(1, codigo);
                this.sqlConsulta.executeUpdate();
                this.desconectar();
            }catch(SQLException e){
                System.out.println("Error al borrar " + e.getMessage());
            }
        }else{
            System.out.println("No fue posible conectarse");
        }

    }

    @Override
    public boolean isExiste(String id_Clientes) throws Exception {
        boolean confirm = false;
        if(this.conectar()){
            String consulta = "Select * from Clientes where id_Clientes = ?";
            this.sqlConsulta = conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, id_Clientes);
            this.registros = this.sqlConsulta.executeQuery();
            if(this.registros.next()){
                confirm = true;
            }
        }
        this.desconectar();
        return confirm;
    }

    @Override
    public ArrayList listar() throws Exception {
        ArrayList<Clientes> lista = new ArrayList<Clientes>();
        Clientes user;
        if(this.conectar()){
            String consulta = "Select * from clientes order by nombres";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.registros = this.sqlConsulta.executeQuery();
            
            while(this.registros.next()){
                user = new Clientes();
                user.setIdCliente(this.registros.getInt("id_Clientes"));
                user.setNombres(this.registros.getString("nombres"));
                user.setApellidoPa(this.registros.getString("apellidoPa"));
                user.setApellidoMa(this.registros.getString("apellidoMa"));
                user.setTelefono(this.registros.getString("telefono"));
                user.setDireccion(this.registros.getString("direccion"));
               
                
                lista.add(user);
            }
        }
        this.desconectar();
        return lista;
    }

    @Override
    public Object buscar(String nombre) throws Exception {
        Clientes user = new Clientes();
        if(conectar()){
            String consulta = "SELECT * from clientes WHERE nombres = ?";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, nombre);

            this.registros = this.sqlConsulta.executeQuery();
            if(this.registros.next()){
                user.setNombres(this.registros.getString("nombres"));
                user.setApellidoPa(this.registros.getString("apellidoPa"));
                user.setApellidoMa(this.registros.getString("apellidoMa"));
                user.setDireccion(this.registros.getString("direccion"));
                user.setTelefono(this.registros.getString("telefono"));
            }
        }
        this.desconectar();
        return user;
    }

    @Override
    public void insertarCita(Object objeto) throws Exception {
        Citas user = new Citas();
        user = (Citas) objeto;
        String consulta = "insert into citas(FK_cliente,fecha,descripcion)" +"values (?, ?, ?)";
        if(this.conectar()){
            try{
                System.err.println("Se conecto");
                this.sqlConsulta = conexion.prepareStatement(consulta);
                //Asignar valores a la consulta
                //this.sqlConsulta.setInt(1, user.getIdCliente());
                this.sqlConsulta.setInt(1,user.getFkCliete());
                this.sqlConsulta.setString(2, user.getFecha());
                this.sqlConsulta.setString(3, user.getDescripcion()); 
                this.sqlConsulta.executeUpdate();
                this.desconectar();
            }
            catch (SQLException e){
                System.err.println("Surgió un error al insertar" + e.getMessage());
            }
        }
        else{
            System.out.println("No fue posible conectarse ");
        }
//throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    
    
}
