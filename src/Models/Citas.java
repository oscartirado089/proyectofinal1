/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Models;

/**
 *
 * @author _Enrique_
 */
public class Citas {
    
    private int fkCliete;
    private int idCitas;
    private String fecha;
    private String descripcion;
    
    //Constructores
    public Citas(int FK, int idCitas, String fecha, String decripcion){
        
        this.idCitas=idCitas;
        this.fkCliete=FK;
        this.fecha=fecha;
        this.descripcion=descripcion;
        
    }
    public Citas(){
        
        
    }

    public int getFkCliete() {
        return fkCliete;
    }

    public void setFkCliete(int fkCliete) {
        this.fkCliete = fkCliete;
    }

    public int getIdCitas() {
        return idCitas;
    }

    public void setIdCitas(int idCitas) {
        this.idCitas = idCitas;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    
}
