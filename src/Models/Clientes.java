/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Models;

/**
 *
 * @author _Enrique_
 */
public class Clientes {
    
    private int idCliente;
    private String nombres;
    private String apellidoPa;
    private String apellidoMa;
    private String telefono;
    private String direccion;
 
    //Constructores
    public Clientes (int idCliente, String nombres, String apellidoPa, String apellidoMa, String telefono, String Direccion){
        
        this.idCliente=idCliente;
        this.nombres=nombres;
        this.apellidoPa=apellidoPa;
        this.apellidoMa=apellidoMa;
        this.telefono=telefono;
        this.direccion=Direccion;
        
    }
    public Clientes(){
        
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidoPa() {
        return apellidoPa;
    }

    public void setApellidoPa(String apellidoPa) {
        this.apellidoPa = apellidoPa;
    }

    public String getApellidoMa() {
        return apellidoMa;
    }

    public void setApellidoMa(String apellidoMa) {
        this.apellidoMa = apellidoMa;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    
    
}
