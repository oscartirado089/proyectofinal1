
package Models;
import Models.Citas;
import Models.dbManejador;
import Models.dbPersistencia;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;
/**
 *
 * @author _Enrique_
 */
public class dbCitas extends dbManejador implements dbPersistencia {
    @Override
    public void insertar(Object objeto) throws Exception {
        Citas user = new Citas();
        user = (Citas) objeto;
        String consulta = "insert into " + "citas (FK_Cliente, fecha, descripcion)" +"values (?, ?, ?)";
        if(this.conectar()){
            try{
                System.err.println("Se conecto");
                this.sqlConsulta = conexion.prepareStatement(consulta);
                //Asignar valores a la consulta
                //this.sqlConsulta.setInt(1, user.getIdCliente());
                this.sqlConsulta.setInt(1, user.getFkCliete());
                this.sqlConsulta.setString(2, user.getFecha());
                this.sqlConsulta.setString(3, user.getDescripcion());
                this.sqlConsulta.executeUpdate();
                this.desconectar();
            }
            catch (SQLException e){
                System.err.println("Surgió un error al insertar" + e.getMessage());
            }
        }
        else{
            System.out.println("No fue posible conectarse ");
        }
    }

    @Override
    public void actualizar(Object objeto) throws Exception {
        Citas user = new Citas();
        user = (Citas) objeto;
        String consulta = "update citas set fecha = ?, descripcion = ?  where FK_cliente = ?";
        if(this.conectar()){
            try{
                System.err.println("Se conecto");
                this.sqlConsulta = conexion.prepareStatement(consulta);
                //Asignar valores a la consulta
                
                this.sqlConsulta.setInt(3, user.getFkCliete());
                this.sqlConsulta.setString(1, user.getFecha());
                this.sqlConsulta.setString(2, user.getDescripcion());
                this.sqlConsulta.executeUpdate();
                this.desconectar();
            }
            catch (SQLException e){
                System.err.println("Surgió un error al insertar" + e.getMessage());
            }
        }
        else{
            System.out.println("No fue posible conectarse ");
        }
    }

    

    @Override
    public boolean isExiste(String FK_cliente) throws Exception {
        boolean confirm = false;
        if(this.conectar()){
            String consulta = "Select * from Citas where FK_Cliente = ?";
            this.sqlConsulta = conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, FK_cliente);
            this.registros = this.sqlConsulta.executeQuery();
            if(this.registros.next()){
                confirm = true;
            }
        }
        this.desconectar();
        return confirm;
    }

    @Override
    public ArrayList listar() throws Exception {
        ArrayList<Citas> lista = new ArrayList<Citas>();
        Citas user;
        if(this.conectar()){
            String consulta = "Select * from citas order by FK_cliente";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.registros = this.sqlConsulta.executeQuery();
            
            while(this.registros.next()){
                user = new Citas();
                user.setIdCitas(this.registros.getInt("id_folio"));
                user.setFkCliete(this.registros.getInt("FK_cliente"));
                user.setFecha(this.registros.getString("fecha"));
                user.setDescripcion(this.registros.getString("descripcion"));
  
                lista.add(user);
            }
        }
        this.desconectar();
        return lista;
    }

    @Override
    public Object buscar(String nombre) throws Exception {
        Citas user = new Citas();
        if(conectar()){
            String consulta = "SELECT * from citas WHERE FK_Cliente = ?";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, nombre);

            this.registros = this.sqlConsulta.executeQuery();
            if(this.registros.next()){
                user.setFkCliete(this.registros.getInt("FK_Cliente"));
                user.setFecha(this.registros.getString("fecha"));
                user.setDescripcion(this.registros.getString("descripcion"));
            }
        }
        this.desconectar();
        return user;
    }

    @Override
    public void borrar(Object objeto, String codigo) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void insertarCita(Object objeto) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }


    
    
}
