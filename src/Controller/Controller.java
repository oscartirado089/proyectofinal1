//correccion de errores
package Controller;

import Views.dlgIngreso;
import Views.dlgRegistro;
import Views.dlgClientes;
import Views.dlgCitas;
import Views.dlgLista;
import Models.Citas;
import Models.Clientes;
import Models.Usuarios;
import Models.dbUsuarios;
import Models.dbCitas;
import Models.dbCliente;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author _Enrique_
 */
public class Controller implements ActionListener {

    private dlgIngreso ingreso;
    private dlgLista lista;
    private dlgRegistro registro;
    private Usuarios users;
    private dbUsuarios db;
    private dbCliente dbC;
    private dlgCitas cita;
    private dbCitas dbCi;

    private dlgClientes clie;
    private Clientes client;
    private Citas citas;
    private boolean A = false;

    public Controller(Usuarios users, dbUsuarios db, dlgIngreso ingreso, dlgLista lista, dlgRegistro registro, dlgCitas cita, dlgClientes clie, dbCliente dbC, Clientes client, Citas citas, dbCitas dbCi) {

        this.users = users;
        this.db = db;
        this.ingreso = ingreso;
        this.lista = lista;
        this.registro = registro;
        this.cita = cita;
        this.clie = clie;
        this.client = client;
        this.dbC = dbC;
        this.citas = citas;
        this.dbCi = dbCi;

        ingreso.btnIngresar.addActionListener(this);
        ingreso.btnRegistrar.addActionListener(this);
        lista.btnCerrarL.addActionListener(this);
        registro.btnGuardarR.addActionListener(this);
        registro.btnLimpiarR.addActionListener(this);
        registro.btnCerrarR.addActionListener(this);
        clie.btnNuevo.addActionListener(this);
        clie.btnAgregar.addActionListener(this);
        clie.btnBuscar.addActionListener(this);
        clie.btnLimpiar.addActionListener(this);
        clie.btnCerrar.addActionListener(this);
        clie.btnCancelar.addActionListener(this);

        lista.btncitaL.addActionListener(this);
        lista.btnclienteL.addActionListener(this);
        clie.btncitaCli.addActionListener(this);
        clie.btnlistaCli.addActionListener(this);
        cita.btnlistaC.addActionListener(this);
        cita.btnclienteC.addActionListener(this);

        cita.btnAgregarCita.addActionListener(this);
        cita.btnBuscarCita.addActionListener(this);
        cita.btnCancelarCita.addActionListener(this);
        cita.btnLimpiarCita.addActionListener(this);
    }

    //Vistas
    public void iniciarIngreso() {
        ingreso.setTitle(":: USUARIOS ::");
        ingreso.setSize(410, 450);
        ingreso.setLocationRelativeTo(null);
        ingreso.setVisible(true);

    }

    public void enabtnClientes() {
        clie.txtNombres.setEnabled(true);
        clie.txtApellidoPa.setEnabled(true);
        clie.txtApellidoMa.setEnabled(true);
        clie.txtDireccion.setEnabled(true);
        clie.txtTelefono.setEnabled(true);

        clie.btnCancelar.setEnabled(true);
        clie.btnLimpiar.setEnabled(true);
        clie.btnAgregar.setEnabled(true);
        clie.btnBuscar.setEnabled(true);
    }

    public void iniciarLista() {
        lista.setTitle(":: USUARIOS ::");
        lista.setSize(460, 350);
        lista.setLocationRelativeTo(null);
        lista.setVisible(true);
    }

    public void iniciarRegistro() {
        registro.setTitle(":: REGISTRO ::");
        registro.setSize(450, 350);
        registro.setLocationRelativeTo(null);
        registro.setVisible(true);
    }

    public void iniciarCitas() {
        cita.setTitle(":: REGISTRO CITAS ::");
        cita.setSize(580, 600);
        cita.setLocationRelativeTo(null);
        cita.setVisible(true);
    }

    public void iniciarClientes() {
        clie.setTitle(":: Registro Clientes ::");
        clie.setSize(560, 580);
        clie.setLocationRelativeTo(null);
        clie.setVisible(true);
    }

    private void closeI() {
        ingreso.dispose();
    }

    private void closeCita() {
        cita.dispose();
    }

    private void closeL() {
        lista.dispose();
    }

    private void closeR() {
        registro.dispose();
    }

    private void closeClie() {
        registro.dispose();
    }

    public void limpiar() {
        registro.txtCContraseñaR.setText("");
        registro.txtContraseñaR.setText("");
        registro.txtCorreoR.setText("");
        registro.txtUsuarioR.setText("");
        A = false;
    }

    public void limpiarClie() {
        clie.txtNombres.setText("");
        clie.txtApellidoPa.setText("");
        clie.txtApellidoMa.setText("");
        clie.txtTelefono.setText("");
        clie.txtDireccion.setText("");
        A = false;
    }

    public void limpiarCitas() {
        cita.txtClienteCita.setText("");
        cita.dateFecha.setDate(null);
        cita.txtDescripcion.setText("");

        A = false;
    }

    public void cargarDatos() {
        DefaultTableModel modelo = new DefaultTableModel();
        ArrayList<Usuarios> listas = new ArrayList<>();
        try {
            listas = db.listar();
        } catch (Exception ex) {

        }
        modelo.addColumn("id_Clientes");
        modelo.addColumn("nombre");
        modelo.addColumn("correo");
        for (Usuarios usu : listas) {
            modelo.addRow(new Object[]{usu.getIdUsuario(), usu.getUsuario(), usu.getCorreo()});
        }
        lista.tblLista.setModel(modelo);
    }

    public void cargarDatosC() {
        DefaultTableModel modelo = new DefaultTableModel();
        ArrayList<Citas> listasC = new ArrayList<Citas>();
        try {
            listasC = dbCi.listar();
        } catch (Exception ex) {

        }
        modelo.addColumn("id_Citas");
        modelo.addColumn("Fk_Clientes");
        modelo.addColumn("fecha");
        modelo.addColumn("descripcion");
        for (Citas citas : listasC) {
            modelo.addRow(new Object[]{citas.getIdCitas(), citas.getFkCliete(),
                citas.getFecha(), citas.getDescripcion()});
        }
        cita.jtbDate1.setModel(modelo);
    }

    public void cargarDatosClie() {
        DefaultTableModel modelo = new DefaultTableModel();
        ArrayList<Clientes> listas = new ArrayList<>();
        try {
            listas = dbC.listar();
        } catch (Exception ex) {

        }
        modelo.addColumn("id_Clietes");
        modelo.addColumn("nombres");
        modelo.addColumn("apellidoPa");
        modelo.addColumn("apellidoMa");
        modelo.addColumn("telefono");
        modelo.addColumn("direccion");
        for (Clientes cliee : listas) {
            modelo.addRow(new Object[]{cliee.getIdCliente(), cliee.getNombres(),
                cliee.getApellidoPa(), cliee.getApellidoMa(), cliee.getTelefono(), cliee.getDireccion()});
        }
        clie.listaa.setModel(modelo);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == ingreso.btnIngresar) {
            try {
                Usuarios users = new Usuarios();
                if (db.isExiste(ingreso.txtUsuario.getText())) {
                    users = (Usuarios) db.buscar(ingreso.txtUsuario.getText());
                    if (ingreso.txtContraseña.getText().equals(users.getContraseña())) {
                        JOptionPane.showMessageDialog(null, "Sucessfull Login");
                        ingreso.dispose();
                        closeI();
                        iniciarClientes();
                        //db.listar();
                    } else {
                        JOptionPane.showMessageDialog(null, "Password or User Incorrect");
                        return;
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Usuario no registrado");
                    return;
                }
            } catch (SQLException d) {
                d.printStackTrace();
            } catch (Exception ex1) {
                System.err.println("Surgio un error" + ex1);
            }
        }
        if (e.getSource() == ingreso.btnRegistrar) {
            int option = JOptionPane.showConfirmDialog(ingreso, "¿Desea crear un nuevo usuario?",
                    "Decide", JOptionPane.YES_NO_OPTION);
            if (option == JOptionPane.YES_NO_OPTION) {
                ingreso.dispose();
                closeI();
                iniciarRegistro();
            }
        }
        if (e.getSource() == registro.btnGuardarR) {
            try {
                if ("".equalsIgnoreCase(registro.txtUsuarioR.getText())) {
                    throw new IllegalArgumentException("Campo Vacio. \nEscribe un usuario");
                }
                if (db.isExiste(registro.txtUsuarioR.getText())) {
                    JOptionPane.showMessageDialog(registro, "El usuasrio ya existe");
                    return;
                }
                if ("".equalsIgnoreCase(registro.txtCorreoR.getText())) {
                    throw new IllegalArgumentException("Campo Vacio. \nEscribe un correo");
                }
                if ("".equalsIgnoreCase(registro.txtCContraseñaR.getText())) {
                    throw new IllegalArgumentException("Campo vacio. \nEscribe una contraseña");
                }
            } catch (IllegalArgumentException ex) {
                JOptionPane.showMessageDialog(registro, "SURGIO UN ERROR: " + ex.getMessage());
                return;
            } catch (Exception ex2) {
                JOptionPane.showMessageDialog(registro, "Error: " + ex2.getMessage());
                return;
            }
            try {
                users.setUsuario(registro.txtUsuarioR.getText());
                users.setCorreo(registro.txtCorreoR.getText());
                users.setContraseña(registro.txtContraseñaR.getText());
                if (registro.txtContraseñaR.getText().equals(registro.txtCContraseñaR.getText())) {
                    users.setCtr2(registro.txtCContraseñaR.getText());
                } else {
                    JOptionPane.showMessageDialog(null, "Las contraseñas no coinciden");
                    return;
                }

                
                if (A) {
                    db.insertar(users);
                    JOptionPane.showMessageDialog(null, "Registro Guardado");
                    limpiar();
                    cargarDatos();
                }
            } catch (SQLException d) {
                d.printStackTrace();
            } catch (Exception ex1) {
                System.err.println("Surgió un error" + ex1);
            }
        }
        if (e.getSource() == registro.btnLimpiarR) {
            limpiar();
        }

        if (e.getSource() == registro.btnCerrarR) {
            int option = JOptionPane.showConfirmDialog(registro, "¿Regresar al Login?",
                    "Decide", JOptionPane.YES_NO_OPTION);
            if (option == JOptionPane.YES_NO_OPTION) {
                registro.dispose();
                closeR();
                iniciarIngreso();
            }
        }

        if (e.getSource() == lista.btnCerrarL) {
            lista.dispose();
            iniciarIngreso();
        }
        if (e.getSource() == clie.btnNuevo) {

            clie.txtNombres.setEnabled(true);
            clie.txtApellidoPa.setEnabled(true);
            clie.txtApellidoMa.setEnabled(true);
            clie.txtDireccion.setEnabled(true);
            clie.txtTelefono.setEnabled(true);

            clie.btnCancelar.setEnabled(true);
            clie.btnLimpiar.setEnabled(true);
            clie.btnAgregar.setEnabled(true);
            clie.btnBuscar.setEnabled(true);

        }
        if (e.getSource() == clie.btnAgregar) {

            try {
                if ("".equalsIgnoreCase(clie.txtNombres.getText())) {
                    throw new IllegalArgumentException("Campo Vacio. \nEscribe un nombre");
                }
                if ("".equalsIgnoreCase(clie.txtApellidoPa.getText())) {
                    throw new IllegalArgumentException("Campo Vacio. \nEscribe el apellido paterno");
                }
                if ("".equalsIgnoreCase(clie.txtApellidoMa.getText())) {
                    throw new IllegalArgumentException("Campo Vacio. \nEscribe el apellido materno");
                }
                if ("".equalsIgnoreCase(clie.txtTelefono.getText())) {
                    throw new IllegalArgumentException("Campo Vacio. \nEscribe un telefono");
                }
                if (dbC.isExiste(clie.txtTelefono.getText())) {
                    JOptionPane.showMessageDialog(registro, "El telefono ya existe");
                    return;
                }
                if ("".equalsIgnoreCase(clie.txtDireccion.getText())) {
                    throw new IllegalArgumentException("Campo Vacio. \nEscribe una direccion");
                }
            } catch (IllegalArgumentException ex) {
                JOptionPane.showMessageDialog(registro, "SURGIO UN ERROR: " + ex.getMessage());
                return;
            } catch (Exception ex) {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            }
            // client.setIdCliente(Integer.valueOf(clie.txtId.getText());
            client.setNombres(clie.txtNombres.getText());
            client.setApellidoPa(clie.txtApellidoPa.getText());
            client.setApellidoMa(clie.txtApellidoMa.getText());
            client.setTelefono(clie.txtTelefono.getText());
            client.setDireccion(clie.txtDireccion.getText());
            A = true;
            if (A) {
                try {
                    //dbC.insertar(users);
                    dbC.insertar(client);
                } catch (Exception ex) {
                    Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
                }
                JOptionPane.showMessageDialog(null, "Registro Guardado");
                limpiarClie();
                cargarDatosClie();
            }

        }
        if (e.getSource() == clie.btnCerrar) {
            int option = JOptionPane.showConfirmDialog(registro, "¿Regresar al Menu?",
                    "Decide", JOptionPane.YES_NO_OPTION);
            if (option == JOptionPane.YES_NO_OPTION) {
                clie.dispose();
                closeClie();
                iniciarIngreso();
            }
        }
        if (e.getSource() == clie.btnCancelar) {
            limpiarClie();

        }
        if (e.getSource() == clie.btnLimpiar) {
            limpiarClie();
        }
        if (e.getSource() == clie.btnBuscar) {
            try {
                client = (Clientes) dbC.buscar(clie.txtNombres.getText());
                clie.setName(clie.txtNombres.getText());
                limpiarClie();
                dbC.buscar(clie.txtNombres.getText());
                clie.txtNombres.setText(client.getNombres());
                clie.txtApellidoPa.setText(client.getApellidoPa());
                clie.txtApellidoMa.setText(client.getApellidoMa());
                clie.txtTelefono.setText(String.valueOf(client.getTelefono()));
                clie.txtDireccion.setText(client.getDireccion());
                if (dbC.buscar(clie.txtNombres.getText()).equals(-1)) {
                    JOptionPane.showMessageDialog(clie, "No se encontro");
                } else {
                    JOptionPane.showMessageDialog(clie, "Se encontro");
                    enabtnClientes();
                    clie.txtNombres.setText(client.getNombres());
                    clie.txtApellidoPa.setText(client.getApellidoPa());
                    clie.txtApellidoMa.setText(client.getApellidoMa());
                    clie.txtTelefono.setText(String.valueOf(client.getTelefono()));
                    clie.txtDireccion.setText(client.getDireccion());
                }

            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(clie, "No se pudo Buscar, surgio el siguiente error: " + ex.getMessage());
            } catch (Exception ex2) {
                JOptionPane.showMessageDialog(clie, "Surgio el siguiente error: " + ex2.getMessage());
            }
        }

        if (e.getSource() == cita.btnAgregarCita) {
            try {
                /*("".equalsIgnoreCase(clie.txtId.getText())){
                    throw new IllegalArgumentException("Campo Vacio. \nEscribe un id");
                }*/
                if ("".equalsIgnoreCase(cita.txtClienteCita.getText())) {
                    throw new IllegalArgumentException("Campo Vacio. \nEscribe el cliente");
                }

                if ("".equalsIgnoreCase(cita.txtDescripcion.getText())) {
                    throw new IllegalArgumentException("Campo Vacio. \nEscribe la descripcion");
                }
            } catch (IllegalArgumentException ex) {
                JOptionPane.showMessageDialog(registro, "SURGIO UN ERROR: " + ex.getMessage());
                return;
            } catch (Exception ex) {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            }
            // client.setIdCliente(Integer.valueOf(clie.txtId.getText());
            citas.setFkCliete(Integer.parseInt(cita.txtClienteCita.getText()));
            citas.setDescripcion(cita.txtDescripcion.getText());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String date = sdf.format(cita.dateFecha.getDate());
            citas.setFecha(date);
            try {
                if (dbCi.isExiste(Integer.toString(citas.getFkCliete()))) {
                    if (A) {
                        try {
                            //dbC.insertar(users);
                            dbCi.actualizar(citas);
                            cargarDatosC();
                            A = false;
                        } catch (Exception ex) {
                            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        JOptionPane.showMessageDialog(null, "Registro Guardado");
                        limpiarClie();
                    } else {
                        JOptionPane.showMessageDialog(null, "Registro repetido");
                    }
                } else {
                    try {
                        //dbC.insertar(users);
                        dbCi.insertarCita(citas);
                    } catch (Exception ex) {
                        Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    cargarDatosC();
                }
            } catch (IllegalArgumentException ex) {
                JOptionPane.showMessageDialog(registro, "SURGIO UN ERROR: " + ex.getMessage());
                return;
            } catch (Exception ex) {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        if (e.getSource() == cita.btnBuscarCita) {
            try {
                cargarDatosC();
                citas = (Citas) dbCi.buscar(cita.txtClienteCita.getText());
                citas.setFkCliete(Integer.valueOf(cita.txtClienteCita.getText()));
                limpiarCitas();
                dbCi.buscar(cita.txtClienteCita.getText());
                cita.txtClienteCita.setText(Integer.toString(citas.getFkCliete()));
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                cita.txtDescripcion.setText(citas.getDescripcion());
                cita.dateFecha.setDate(sdf.parse(citas.getFecha()));
                if (dbCi.buscar(cita.txtClienteCita.getText()).equals(-1)) {
                    JOptionPane.showMessageDialog(cita, "No se encontro");
                } else {
                    A = true;
                    JOptionPane.showMessageDialog(cita, "Se encontro");
                    cita.txtClienteCita.setText(Integer.toString(citas.getFkCliete()));
                    cita.dateFecha.setDate(sdf.parse(citas.getFecha()));
                    cita.txtDescripcion.setText(citas.getDescripcion());

                }
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(clie, "No se pudo Buscar, surgio el siguiente error: " + ex.getMessage());
            } catch (Exception ex2) {
                JOptionPane.showMessageDialog(clie, "Surgio el siguiente error: " + ex2.getMessage());
            }
        }
        if (e.getSource() == cita.btnlistaC) {
            cita.dispose();
            closeCita();
            iniciarLista();
            cargarDatosC();
            cargarDatos();
        }
        if (e.getSource() == cita.btnclienteC) {
            cita.dispose();
            closeCita();
            iniciarClientes();
        }
        if (e.getSource() == clie.btncitaCli) {
            clie.dispose();
            closeClie();
            cargarDatosC();
            iniciarCitas();
        }
        if (e.getSource() == clie.btnlistaCli) {
            clie.dispose();
            closeClie();
            iniciarLista();
        }
        if (e.getSource() == lista.btncitaL) {
            lista.dispose();
            closeL();
            iniciarCitas();
        }
        if (e.getSource() == lista.btnclienteL) {
            lista.dispose();
            closeL();
            iniciarClientes();
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        // TODO code application logic here
        Usuarios users = new Usuarios();
        dbUsuarios db = new dbUsuarios();
        Clientes cliee = new Clientes();
        dbCliente dbClie = new dbCliente();
        Citas cit = new Citas();
        dbCitas dbCita = new dbCitas();
        dlgIngreso ingreso = new dlgIngreso();
        dlgLista lista = new dlgLista();
        dlgRegistro registro = new dlgRegistro();
        dlgCitas cita = new dlgCitas();
        dlgClientes clie = new dlgClientes();

        Controller contra = new Controller(users, db, ingreso, lista, registro, cita, clie, dbClie, cliee, cit, dbCita);
        contra.cargarDatos();
        contra.cargarDatosClie();
        contra.iniciarIngreso();

    }

}
