/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package Views;

/**
 *
 * @author Enrique
 */
public class dlgLista extends javax.swing.JFrame {

    /**
     * Creates new form dlgLista
     */
    public dlgLista() {
        initComponents();
    }

    private dlgLista lista;
    private dlgClientes clie;
    private dlgCitas cita;
  

    public void iniciarLista() {
        lista.setTitle(":: USUARIOS ::");
        lista.setSize(450, 350);
        lista.setLocationRelativeTo(null);
        lista.setVisible(true);
    }
    
    public void iniciarCitas() {
        cita.setTitle(":: REGISTRO CITAS ::");
        cita.setSize(450, 350);
        cita.setLocationRelativeTo(null);
        cita.setVisible(true);
    }
    
    public void iniciarClientes() {
        clie.setTitle(":: Registro Clientes ::");
        clie.setSize(540, 570);
        clie.setLocationRelativeTo(null);
        clie.setVisible(true);
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tblLista = new javax.swing.JTable();
        btnCerrarL = new javax.swing.JButton();
        btncitaL = new javax.swing.JButton();
        btnclienteL = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);

        tblLista.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Id", "Usuario", "Correo"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tblLista);

        getContentPane().add(jScrollPane1);
        jScrollPane1.setBounds(30, 50, 385, 170);

        btnCerrarL.setText("Cerrar");
        getContentPane().add(btnCerrarL);
        btnCerrarL.setBounds(180, 230, 72, 23);

        btncitaL.setText("Citas");
        btncitaL.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncitaLActionPerformed(evt);
            }
        });
        getContentPane().add(btncitaL);
        btncitaL.setBounds(270, 10, 72, 23);

        btnclienteL.setText("Clientes");
        btnclienteL.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnclienteLActionPerformed(evt);
            }
        });
        getContentPane().add(btnclienteL);
        btnclienteL.setBounds(350, 10, 72, 23);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btncitaLActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncitaLActionPerformed
        
    }//GEN-LAST:event_btncitaLActionPerformed

    private void btnclienteLActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnclienteLActionPerformed
        
    }//GEN-LAST:event_btnclienteLActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(dlgLista.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(dlgLista.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(dlgLista.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(dlgLista.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new dlgLista().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btnCerrarL;
    public javax.swing.JButton btncitaL;
    public javax.swing.JButton btnclienteL;
    private javax.swing.JScrollPane jScrollPane1;
    public javax.swing.JTable tblLista;
    // End of variables declaration//GEN-END:variables
}
